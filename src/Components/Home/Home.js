import React, { Component } from 'react';
import '../../App.css';
import { Login } from '../Login';
import { PreviewForm } from './PreviewForm'
import { UserForm } from './UserForms';
import { PageHeader } from '../PageHeader';
import { PageHeader1 } from '../PageHeader1';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'

export class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
        values: [{question:'', inputType:'text',options:[],required:false}],
        formTitle:'', 
        Description:'', 
        startDate:'', 
        endDate:'', 
        show:false,
        showPreview:false
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handlePreview = this.handlePreview.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.createRadioOptions = this.createRadioOptions.bind(this);
    this.getAddOptionButton = this.getAddOptionButton.bind(this);
    this.getRemoveOptionButton = this.getRemoveOptionButton.bind(this);
    document.title="Feedback Portal - Home";
    }

    createAddButton(i) {

        if (i === 0 && (this.state.values.length === 1)) {
            return (<div className="col col-sm-2">
                <input type='button' value='+' className="btn btn-default btn-xs" onClick={this.addClick.bind(this, this.state.values.length)}
                /></div>);
        }
        if (i === (this.state.values.length - 1)) {
            return (<div className="col col-sm-2">
                <input type='button' value='-' className="btn btn-default btn-xs" onClick={this.removeClick.bind(this, i)} />
                <input type='button' value='+' className="btn btn-default btn-xs" onClick={this.addClick.bind(this, this.state.values.length)}
                    style={{ marginLeft: '10px' }} /></div>);
        }
        return (<div className="col col-sm-2">
            <input type='button' value='-' className="btn btn-default btn-xs" onClick={this.removeClick.bind(this, i)} />
        </div>);
    }

    addNewRadioButton(i)
    {
        let values = [...this.state.values];
        values[i].options.push('');
        this.setState({ values });
    }

    removeRadioButton(i,j)
    {
       let values = [...this.state.values];
      values[i].options.splice(j,1)
       this.setState({ values });
    }
    getAddOptionButton(i,j)
    {
        if(j=== this.state.values[i].options.length-1)
        return(
            <div className="col col-sm-2">
            
            <input type='button' className="btn btn-secondary btn-xs button-home" value='add option' onClick={this.addNewRadioButton.bind(this, i)} /></div>
        )
       
    
    }

    getRemoveOptionButton(i,j)
    {
       // console.log('get remove options button');
        if(this.state.values[i].options.length>2)
        return(
            <div className="col col-sm-2">
            
            <input type='button' className="btn btn-warning btn-xs button-home" value='remove option' onClick={this.removeRadioButton.bind(this, i,j)} /></div>
        )
       
    
    }
    createRadioOptions(i) {
      //  console.log("create radio options");
       // var a=this.state.values[i].options;
       // console.log(a);
///console.log('creating radio options');

        if (this.state.values[i].inputType === 'radio') {

            return (
               
                    this.state.values[i].options.map((el, j) =>
                    
                    <div className='form-group col'  >
                        
                            <div className="row">
                            <input type="radio" name={j} /> <input type="text" className="form-control col-md-6" placeholder={'Value ' + (j+1)} value={el||''} onChange={this.handleRadioValueChange.bind(this, i,j)} required style={{marginLeft: '10px'}} />
                            </div>
                       {this.getAddOptionButton(i,j)} 
                      
                       {this.getRemoveOptionButton(i,j)}
                       </div>
              
                            ))  }
    }
        
    createUI(){
        return this.state.values.map((el, i) => 
            <div key={i}>
            <div className="row"  onChange={this.handleChange.bind(this, i)} >
        


                <div className="col col-md-8">
                <textarea className="form-control" value={el.question||''} placeholder="Enter Your Question" rows="2" required/>
                </div>
                <div style={{width:"130px"}}>
                    <select ref="selectMark" className="mark-selector form-control" >
                    <option value="text">Text</option>
                    <option value="largeText">Large Text</option>
                    <option value="number">Number</option>
                    <option value="date">Date</option>
                    <option value="radio" >Radio</option>
                    <option value="rating" >Rating</option>
                    </select>
                </div>
                {this.createAddButton(i)}
                </div>
                {this.createRadioOptions(i)}
                <br/>
            </div>  
        )
    }

    handleChange(i, event) {
        let values = [...this.state.values];
        if(event.target.nodeName==='TEXTAREA')
        {
            values[i].question = event.target.value;
        }
        else if(event.target.nodeName==='SELECT')
        {
            values[i].inputType = event.target.value;
            if(event.target.value === 'radio')
            {
                values[i].options.push('');
                values[i].options.push('');
            }
            else
            {
                values[i].options=[];
            }
           
        }
        else
        {
            values[i].required=! values[i].required
        }

        this.setState({ values });
    }

    handleRadioValueChange(i,j, event) {
       //console.log("value change");
      // console.log(i);
     //  console.log(j);
      // console.log(event.target.value);
       let values = [...this.state.values];
       values[i].options[j]=(event.target.value);
       this.setState({ values });
    }

    

    handleChangeBasicDetails = (event) => 
    {
        this.setState({
            [event.target.name]:event.target.value
        })
    }
    
    addClick(i,event){
        let values = [...this.state.values];
        values[i]={question:'', inputType:'text', options:[],required:false}
        this.setState(prevState => ({ values: [...prevState.values, values[i]]}))
    }
    
    removeClick(i){
        let values = [...this.state.values];
        
        values.splice(i,1);
        this.setState({ values });
    }
    
    handleSubmit(event) {
       event.preventDefault();
       // console.log(this.state.values);
       
    fetch('http://localhost:8080/form', {
        method: 'POST',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            name: this.state.formTitle,
            user_id: localStorage.getItem( 'brid' ),
            description: this.state.Description,
            start_date: this.state.startDate,
            end_date: this.state.formTitle,
            questions:this.state.values
        })
    }).then(res => res.json())
    .then(
        function(response) {
            console.log('Success:',response.status)
            if(response.status==='success')
            {
                confirmAlert({
                    message: 'Form saved successfully.',
                    buttons: [
                      {
                        label: 'Ok',
                        onClick: () => {
                            window.location.reload();
        
                        }
                      }
                    ]
                  })
          
            }
            else
            {
                //window.alert("Form couldn't be saved\nTry again after some time.");
            }
        
        }.bind(this));   
    
    }

    handlePreview(event) {
        // event.preventDefault();
        // window.open("");

        this.setState(
            {
                ...this.state,
                showPreview:!this.state.showPreview
            }
        )
    }

    handleLogin(event) {
        // event.preventDefault();
        // window.open("");

        this.setState(
            {
                ...this.state,
                show:!this.state.show
            }
        )
    }

handleLogout(event)
{
localStorage.removeItem('brid');
window.location.reload();
}
    render() {
        if(localStorage.getItem( 'brid' )!=null)
        {
        
        
        return(
            <div className="app">
          <PageHeader1/>
            <div className="row" style={{marginLeft:'6.5%', marginTop:'1%'}}>
                <div className="col-sm-8 shadow-sm p-3 mb-5  rounded" style={{backgroundColor: '#f2f2f2'}} >
                <form className="form-horizontal" onSubmit={this.handleSubmit}>
                    
                    <div className="form-group">
                    <input type="text" className="form-control" name="formTitle" placeholder="Title" onChange={this.handleChangeBasicDetails} required/>
                    </div>

                    <div className="form-group">
                    <textarea className="form-control" placeholder="Description" rows="2" name="Description" onChange={this.handleChangeBasicDetails} required/>
                    </div>

                    <div className="row">
                    <div className="col-sm-4">
                        <input type="date" name="startDate" className="form-control" onChange={this.handleChangeBasicDetails} required/>
                    </div>

                    <div className="col-sm-4">
                            <input type="date" name="endDate" className="form-control" onChange={this.handleChangeBasicDetails} required/>
                    </div>
                    </div>

                    <hr/>  


                    {this.createUI()}  

                    <input type="submit" className="btn btn-primary" value="Save Form" />
                    <button type="button" className="btn btn-secondary" style={{marginLeft: '10px'}} onClick={this.handlePreview}> Preview Form</button>
                    
                </form>
                </div>
                <div className="col-sm-3 shadow-sm p-3 mb-5  rounded" style={{backgroundColor: '#f2f2f2',marginLeft:'10px'}} >
                        History section
                    
                        <UserForm/>
                    </div>
                <PreviewForm showPreview={this.state.showPreview} 
                                onClose={this.handlePreview} 
                                onSave={this.handleSubmit}
                                title={this.state.formTitle}
                                description={this.state.Description}
                                values={this.state.values}
                                startDate={this.state.startDate}
                                endDate={this.state.endDate}>
                </PreviewForm>
                            
            </div>
            </div>
        
        );
        }
        else
        {
        return(
            <div>
            <Login show="true"  onClose={this.handleLogin} />
            </div>
        );
        }
    
    }
}

export default Home;
