import React from 'react'
import ShareIcon from '../../Images/shareIcon.png';
import DeleteIcon from '../../Images/deleteIcon.png';
import ViewFormIcon from '../../Images/viewIcon.png';
import ViewResponseIcon from '../../Images/responseIcon.png';
import { ShareFormDialog } from './ShareFormDialog';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'
export class UserForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            forms: [],
            showShareFormDialog:false
        };
        this.handleShareFormDialog = this.handleShareFormDialog.bind(this);
    }

    componentDidMount() {
        var url = 'http://localhost:8080/form/' + localStorage.getItem('brid');
        fetch(url, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        })
            .then(res => res.json())
            .then(json => {
                this.setState(
                    {
                        forms: json,
                    });
                console.log(this.state.forms);
            });

    }

    viewForm(id) {


        var url = 'http://localhost:3000/singleform:' + id;
        window.open(url);
    }

    showResponse(id) {

        var url = 'http://localhost:3000/Response:' + id;
        window.open(url);
    }

    deleteForm(id) {

        confirmAlert({
            message: 'Are you sure you want to delete the form?',
            buttons: [
              {
                label: 'Yes',
                onClick: () => {
                    fetch('http://localhost:8080/deleteForm/' + id, {
                        method: 'GET',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                        },
                    }).then(res => res.json())
                        .then(
                            function (response) {
                                console.log('Success:', response.status)
                                confirmAlert({
                                    message: 'Form deleted successfully.',
                                    buttons: [
                                      {
                                        label: 'Ok',
                                        onClick: () => {
                                            window.location.reload();
                        
                                        }
                                      }
                                    ]
                                  })
            
                            }.bind(this));

                }
              },
              {
                label: 'No',
               
              }
            ]
          })
    

    }

    handleShareFormDialog(id) {
        // event.preventDefault();
        // window.open("");
        console.log(id);
        var url='http://localhost:3000/singleform:' + id;
  
        this.setState(
            {
                ...this.state,
                showShareFormDialog:!this.state.showShareFormDialog,
                selectedFormLink: url
            }
        )

   

       // console.log(this.state.showShareFormDialog)
      }

    render() {
        return (

<div>
            <ol>
                {

                    this.state.forms.map(item => (
                        <li key={item.id} data-id={item.id}>
                        <label style={{  width: '65%' }}>{item.name}</label>
                        <img src={ViewFormIcon} title="Click to view form" style={{cursor: 'pointer'}} onClick={() => this.viewForm(item.id)} width="15" />
                        &nbsp;
                        <img src={ShareIcon} title="Click to share form" style={{cursor: 'pointer'}}  onClick={() => this.handleShareFormDialog(item.id)}  width="15" />
                        &nbsp;
                    <img src={ViewResponseIcon} title="Click to view responses" style={{cursor: 'pointer'}} onClick={() => this.showResponse(item.id)} width="15" />
                        &nbsp;
                   
                      
                    <img src={DeleteIcon} title="Click to delete form" style={{cursor: 'pointer'}} onClick={() => this.deleteForm(item.id)} width="15" />
                    </li>


                    ))
                }
            </ol>
            
<ShareFormDialog
formLink={this.state.selectedFormLink}
showDialog={this.state.showShareFormDialog}
onClose={this.handleShareFormDialog} 
/>
            </div>
        );
    }
}

export default UserForm;