import React from 'react';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import CopyToClipboardIcon from '../../Images/copy-content (1).png';
const backDropStyle = {
  position: 'fixed',
  top: 0,
  bottom: 0,
  right: 0,
  left: 0,
  backgroundColor: 'rgba(0,0,0,0.3)',
  padding: 50
}

const formStyle = {
  backgroundColor: '#fff',
  borderRadius: 5,
  maxWidth: 500,
  maxHeight: 500,
  marginTop: '50px',
  marginLeft: '25%',
  padding: 30,
  position: 'relative',
}


export class ShareFormDialog extends React.Component {
  state = {
    value: '',
    copied: false,
  };
 
  onClose = (e) => {
       
    this.props.onClose && this.props.onClose(e);
    
}

  render() {
    const formLink = this.props.formLink;
       console.log(this.props)
    if (!this.props.showDialog)
    return null;

    return (
      <div style={backDropStyle}>
        <div style={formStyle}>
      <div className='form-group'>
        <input className='form-control' value={formLink}/>
 
       
 <br/>
        <CopyToClipboard text={formLink}>
          <button className="btn btn-secondary">Copy to clipboard</button>
        </CopyToClipboard>
       

          <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={(e) => { this.onClose(e) }} style={{marginLeft:'10px'}}>Close</button>
      </div>
      </div></div>
    );
  }
}

export default ShareFormDialog;