import React from 'react'
import StarRatingComponent from 'react-star-rating-component';

//import Slider from 'react-rangeslider'

// To include the default styles
//import 'react-rangeslider/lib/index.css'
const backDropStyle = {
    position: 'fixed',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: 'rgba(0,0,0,0.3)',
    padding: 50
}

const formStyle = {
    backgroundColor: '#fff',
    borderRadius: 5,
    Width: 700,
    maxHeight: 500,
    margin: '0 auto',
    padding: 30,
    position: 'relative',
    overflowY: 'auto'
}

const footerStyle = {
    position: 'absolute',
    top: 0,
    right: 0
}


export class PreviewForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            volume: 0
        };
    }
    onClose = (e) => {
        this.props.onClose && this.props.onClose(e);
    }


    onSave = (e) => {
        console.log("save");
        this.props.onSave && this.props.onSave(e);
        //  window.location.reload();
    }

    handleOnChange = (value) => {
        this.setState({
            volume: value
        })
    }

    getInputBox(item) {

        console.log(item.question);
        if (item.inputType === "largeText") {
            return (<div className="col-md-6"> <textarea className="form-control" rows="2" /></div>);

        }
        if(item.inputType ==="rating")
            {
                return(<div className='star'>

                <StarRatingComponent 
                    name="rate1" 
                    starCount={5}
                    starColor="rgb(0, 174, 239)"
                   
                />
                 </div>);
            }
       
        if(item.inputType === 'radio')
        { console.log(item.question);
            return (
               
               item.options.map((el, j) =>
                
                <div className='form-group radio-options'  >
                    
                    <input type="radio" name={item.question || 1} value={el}/> <label> {el} </label>
                 
                   </div>
          
                        ))
        }
        return (<div className="col-md-3"><input className="form-control" type={item.inputType} /></div>);
    }

    render() {
        const { title, description, startDate, endDate, values } = this.props;
        // var volume=0
        //console.log(values);
        if (!this.props.showPreview)
            return null;


        return (
            <div style={backDropStyle}>
                <div style={formStyle}>
                    <div style={{ textAlign: "center" }}>
                        <label style={{ fontSize: '18px' }}>{title}</label>
                    </div>
                    <div style={{ textAlign: "center" }}>
                        <label  style={{ fontSize: '14px' }}>{description}</label>
                    </div>
                    <ol style={{marginLeft:'5%'}}>
                        {
                            values.map((item,i) => (
                                <li key={item.question} >
                                    {item.question}

                                    <div className="form-group row" style={{ margin: "10px" }}>
                                        {this.getInputBox(item)}
                                    </div>
                                </li>
                            ))
                        }
                    </ol>
                    <input type="submit" className="btn btn-primary" style={{marginLeft:'6.5%'}} value="Save Form"  onClick={(e) => { this.onSave(e) }} />
                    <button type="button" className="btn btn-secondary" style={{marginLeft:'6.5%'}} data-dismiss="modal" onClick={(e) => { this.onClose(e) }} style={{marginLeft:'10px'}}>Close</button>
                    
                </div>
            </div>
        );
    }
}

export default PreviewForm;