import React, { Component } from 'react';
import { PageHeader } from '../PageHeader';



export class Response extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formid: this.props.match.params.formid,
            responses: [],
            form: [],
            isLoaded: false
        }
    }

    componentDidMount() {
        document.title = "Feedback Portal - Submit Form";

        var url = 'http://localhost:8080/singleform/' + (this.state.formid).substring(1);
        fetch(url)
            .then(res => res.json())
            .then(json => {
                this.setState(
                    {
                        form: json, isLoaded: true,

                    })

                const currentDate = new Date();
            }
            )

        //5b892d4b94a7c21710b88f80
        url = 'http://localhost:8080/getresponses/' + (this.state.formid).substring(1);
        //console.log(url);
        fetch(url)
            .then(res => res.json())
            .then(json => {
                this.setState(
                    {
                        responses: json
                    })
                console.log(this.state.responses)
            });
    }


    showGraph(event) {
        console.log(this.state.form)
        console.log(this.state.responses)
        var data = [];
        (this.state.form.questions).map((item, i) => {
            if (item.inputType === 'radio' || item.inputType === 'rating') {
                const newItem = [{
                    questionName: item.question,
                    questionId: i,
                    answers: []
                }];





                data.push(newItem);
            }

        });




        //console.log(data)
    }

    render() {
        if (this.state.isLoaded) {
            return (

                <div>
                    <PageHeader/>
                    <div style={{marginLeft:'6.5%'}}>
                        <label  style={{ fontSize: '20px', fontWeight: 'bold', padding: '3px' }}>Title: </label>
                        <label  style={{ fontSize: '20px' }}>{this.state.form.name}</label>
                    </div>
                    <div style={{marginLeft:'6.5%', marginTop:'-1%'}}>
                        <label  style={{ fontSize: '20px', fontWeight: 'bold', padding: '3px' }}>Description: </label>
                        <label style={{ fontSize: '20px' }}> {this.state.form.description}</label>
                    </div>



<div class="table-content-class">
<table className='table table-bordered' style={{marginLeft:'6.5%', marginRight:'6.5%', width:'87%'}}>
                        <thead className="thead-light">
                            <tr>
                                {
                                    (this.state.form.questions).map((item, i) => (
                                        <th scope="col" key={i}>{item.question}</th>
                                    ))
                                }

                            </tr>

                        </thead>
                        <tbody>

                            {


                                (this.state.responses).map((item, i) => (

                                    <tr key={i}>
                                        {
                                            (item.answers).map((answer, i) => (

                                                <td key={i}>
                                                    {answer.answer}
                                                </td>
                                            ))
                                        }
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>


</div>
                   
                </div>

            );
        }
        else {
            //console.log("not");
            return ("not loaded");
        }



    }
}

export default Response;
