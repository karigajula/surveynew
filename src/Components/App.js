import React, { Component } from 'react';
import '../App.css';
import Home from './Home/Home'
import SubmitForm from './SubmitFeedback/SubmitForm'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { Response } from './Response/Response';

export class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Route path="/" exact strict render={
            () => {
              return (<Home />);
            }
          } />
          <Route path="/singleform:formid" exact strict render={(props) => (
            <SubmitForm {...props} />
          )} />

          <Route path="/response:formid" exact strict render={(props) => (
            <Response {...props} />
          )} />

        </div>
      </Router>
    );
  }
}

export default App;
