import React, { Component } from 'react';

const backDropStyle = {
    position: 'fixed',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: 'rgba(0,0,0,0.1)',
    padding: 50
}

const formStyle = {
    backgroundColor: '#fff',
    borderRadius: 5,
    maxWidth: 350,
    maxHeight: 300,
    margin: '0 auto',
    padding: 30,
}

export class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            brid: '',
            password: '',
            errors: {}
        };
    }



    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleValidation() {
        let errors = {};
        let formIsValid = true;

        //brid
        if (!this.state.brid) {
            formIsValid = false;
            errors["brid"] = "BRID cannot be empty.";
        }

        //password
        if (!this.state.password) {
            formIsValid = false;
            errors["password"] = "Password cannot be empty.";
        }
        this.setState({ errors: errors });
        return formIsValid;
    }
    onClose = (e) => {
        this.props.onClose && this.props.onClose(e);
    }
    handleSubmit = (event) => {
        if (this.handleValidation()) {
            var loginDetails = JSON.stringify({
                uid: this.state.brid,
                password: this.state.password
            });
            // console.log(loginDetails);
            fetch('http://localhost:8080/loginDetail', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    uid: this.state.brid,
                    password: this.state.password
                })
            }).then(res => res.json())
                .then(
                    function (response) {
                        console.log('Success:', response.status)
                        if (response.status === 'success') {
                            localStorage.setItem('authorized', true);
                            localStorage.setItem('brid', this.state.brid);
                            this.onClose(event)
                        }
                        else {
                            let errors = {};
                            errors["login"] = "Invalid login details.";
                            this.setState({ errors: errors });
                        }
                    }.bind(this));
            /*  this.state.authorized=true;
             var userIds = [{brid:'G01230778',password:'karishma'},
                          {brid:'G01233245',password:'naman'},
                          {brid:'G01229592',password:'rohit'},
                          {brid:'G01212794',password:'shaival'}];
              for(var i=0;i<userIds.length;i++)
              {
                  if(userIds[i].brid===this.state.brid && userIds[i].password===this.state.password)
                  {
                      localStorage.setItem( 'authorized', true );
                      localStorage.setItem( 'brid', this.state.brid );
                      this.setState({authorized: true});
                      console.log(loginDetails); 
                      this.onClose(event)
                      break;
                  }
                  else
                  {
                      let errors = {};
                      errors["login"] = "Invalid login details.";
                      this.setState({errors: errors});
                  }
              }*/

        } else {
            console.log("Login failed!!!");
        }
        event.preventDefault();
    }

    render() {

        const login = (
            <form onSubmit={this.handleSubmit}>
                <div style={backDropStyle}>
                    <div style={formStyle} className="shadow-sm p-3 mb-5 bg-white rounded  text-center">
                        <div >
                            <div className="form-group">
                                <input type="text" className="form-control" name="brid" placeholder="BRID" onChange={this.handleChange} />

                            </div>
                            <div className="form-group">
                                <input type="password" className="form-control" name="password" placeholder="Password" onChange={this.handleChange} />

                            </div>
                            <input type="submit" value="Login" className="btn btn-primary col-sm-6" /><br />

                            <span className="Validation-error-text">{this.state.errors["brid"]}</span><br />
                            <span className="Validation-error-text">{this.state.errors["password"]}</span>
                            <span className="Validation-error-text">{this.state.errors["login"]}</span>
                        </div>
                    </div>
                </div>

            </form>
        );
        if (!this.props.show)
            return null;

        return login;


    }
}

export default Login;
