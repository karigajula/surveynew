import React, { Component } from 'react';
import '../../App.css';
import Login from '../Login'
import StarRatingComponent from 'react-star-rating-component';
import { PageHeader } from '../PageHeader';

export class SubmitForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            form: [],
            answers: [{
                question_number: 0,
                answer: ''
            }],
            isExpired: false,
            isLoaded: false,
            isFormSubmitted:false,
            show:false
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
        this.getInputBox=this.getInputBox.bind(this);
        this.setRequiredFieldValidation=this.setRequiredFieldValidation.bind(this);
    }

    componentDidMount() {
        document.title = "Feedback Portal - Submit Form";
        var url = 'http://localhost:8080/singleform/' + (this.props.match.params.formid).substring(1);
        fetch(url)
            .then(res => res.json())
            .then(json => {
                this.setState(
                    {
                        form: json, isLoaded: true

                    })
                   
                    if(this.state.form.user_id === localStorage.getItem('brid'))
                    {
                     //   document.getElementById("submitButton").disabled = true;
                    }
                    console.log(this.state.form);
                if (new Date(this.state.form.end_date).valueOf() <= new Date().valueOf()) {
                   // document.getElementById("submitButton").disabled = true;
                }

                for (var i = 1; i < this.state.form.questions.length; i++) {
                    const newItem = {
                        question_number: i,
                        answer: ''
                    }
                    this.setState(
                        {
                            answers: [...this.state.answers, newItem]
                        }
                    );
                }
            });
    }

    handleLogin(event) {
        // event.preventDefault();
        // window.open("");
    
        this.setState(
            {
                ...this.state,
                show:!this.state.show
            }
        )
      }

    getInputBox(inputType,i) {

        if (inputType === "largeText") {

            return (<div className="col-md-6"> <textarea className="form-control" rows="2" id={i} /></div>);

        }
            if(inputType ==="rating")
            {
                return(<div className='star'>

                <StarRatingComponent 
                    name="rate1" 
                    starCount={5}
                    starColor="rgb(0, 174, 239)"
                    id={i}
                />
                 </div>);
            }
       
        if(inputType === 'radio')
        {
            return (
               
                this.state.form.questions[i].options.map((el, j) =>
                
                <div className="radio radio-options">
                    
                    
                    <input type="radio" name={i} value={el}  id={i}/> <label> {el} </label>
                    
                   </div>
          
                        ))
        }
       

        return (<div className="col-md-3"><input className="form-control" type={inputType}  id={i}/></div>);
    }

    handleChange(i, event) {
        let answers = [...this.state.answers];
        answers[i].answer = event.target.value;
        this.setState({ answers });
    }

    handleSubmit(event) {
     event.preventDefault();
    console.log(this.state.answers);
         var test = JSON.stringify({
            form_id: (this.props.match.params.formid).substring(1),
            user_id: localStorage.getItem('brid'),
            answers: this.state.answers
        });
       // console.log(test);

        fetch('http://localhost:8080/addresponse', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                form_id: (this.props.match.params.formid).substring(1),
                user_id: localStorage.getItem('brid'),
                answers: this.state.answers
            })
        }).then(res => res.json())
            .then(
                function (response) {
                    console.log('Success:', response.status)
                    if (response.status === 'success') {
                        window.alert("Form saved successfully.");
                        this.setState(
                            {
                                isFormSubmitted: true
                            }
                        );
                        //window.location.reload();
                    }
                    else {
                        //window.alert("Form couldn't be saved\nTry again after some time.");
                    }

                }.bind(this));

    }

    setRequiredFieldValidation()
    {
      
         (this.state.form.questions).map((item, i) => {
             if(document.getElementById(i) != null)
             {
                document.getElementById(i).required=(item.required==='true'?true:false);
             }
       
        })
    }

    render() {
        console.log("hello");
        if(localStorage.getItem( 'brid' )!=null)
    {
        var { form } = this.state;
        if (this.state.isLoaded && this.state.isFormSubmitted==false) {
            return (
               
                <form className="form-horizontal" onSubmit={this.handleSubmit}>
                <PageHeader/>
                    <div  style={{marginLeft:'6.5%'}}>
                    
                        <div style={{ textAlign: "center" }}>
                            <label  style={{ fontSize: '18px' }}>{form.name}</label><br/>
                            <label  style={{ fontSize: '14px' }}>{form.description}</label>
                        </div>
                        <ol>
                            {
                                (form.questions).map((item, i) => (
                                    <li key={i} onChange={this.handleChange.bind(this, i)} >
                                        {item.question}

                                        <div className="form-group row" style={{ margin: "10px" }}>
                                            {this.getInputBox(item.inputType,i)}
                                        </div>
                                    </li>
                                ))
                            }
                        </ol>
                      
                        <input type="submit" className="btn btn-primary" value="Submit Form" id="submitButton" style={{marginLeft:'2%', marginBottom:'1%'}} />
                    </div>
                    {this.setRequiredFieldValidation()}
                </form>
            );
        }
        else if(this.state.isFormSubmitted==true)
        {
            return (
                <div className="col-sm-2 shadow-sm p-3 rounded" style={{ left: '50%'}} >
                 <label style={{color:'green',textAlign:'centre'}}> Thank you for submitting feedback.</label>   
           </div>
            );
        }
        else {
            return (
                <div>
                    Loading...
           </div>
            );
        }
    }
    else
    {
        return(
            <div>
            <Login show="true"  onClose={this.handleLogin} />
            </div>
          );
    }
        
    }
}

export default SubmitForm;
