import React, { Component } from 'react';
import BarclaysLogo from '../Images/barclaysLogo.png';


export class PageHeader1 extends Component {
    handleLogout(event)
    {
    localStorage.removeItem('brid');
    window.location.reload();
    }
    
    render() {
        return( 
            <div className='App-header'>
                <label className='App-title'>
                Feedback Portal
                </label>
                <button className='Button-logout' title='Logout' onClick={this.handleLogout}> <i className="glyphicon glyphicon-user" title='Delete form'/>Logout</button>
               <br/>
                <img className='Barclays-logo' src={BarclaysLogo} alt='Barclays Logo' /> 
            </div>
        );
      
    }
}

export default PageHeader1;